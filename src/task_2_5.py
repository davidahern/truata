from airflow.models import DAG
from airflow.utils.dates import days_ago
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup

with DAG(
  "tasks",
  start_date=days_ago(1),
  schedule_interval=None,
) as dag:
   task_1 = DummyOperator(task_id="task_1")
   with TaskGroup("group1") as group1:
      task_2 = DummyOperator(task_id="task_2")
      task_3 = DummyOperator(task_id="task_3")
        
   with TaskGroup("group2") as group2:
      task_4 = DummyOperator(task_id="task_4")
      task_5 = DummyOperator(task_id="task_5")
      task_6 = DummyOperator(task_id="task_6")
    
   task_1 >> group1 >> group2
